## Statista Backend Code Challenge

**Summary**

- Please add all needed functionality to this small application to fetch the current weather for any given location on the earth.


**Preparations**

- Create an account for the Darksky API here -> https://darksky.net/dev (it's free!)


**Tasks**

- Add useful methods to the WeatherService interface to retrieve the current weather for the user input

- Use this interface in the partial class MainWindow to retrieve the current weather

- Use the darksky api to retrieve the data 

- There is currently no control in place to display the data